import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {Button, Picker, Icon} from 'native-base';
import MapView, {PROVIDER_GOOGLE, Callout, Marker} from 'react-native-maps';

import styles from '../styles/Styles';


export default class StartScreen extends Component {
    static navigationOptions = {
        title: 'Start'
    }

    constructor(props) {
        super(props);

        this.state = {
            activities: [{id:'walking', name:'Walking'}, {id:'running', name:'Running'}, {id:'biking', name:'Biking'}],
            selectedActivity: 'walking',
            latitude: null,
            longitude: null,
            temperature: null,
            skyCondition: ''
        };
    }

    render(){
        return(
            <View style={{flex: 1, flexDirection: 'column'}}>
                <Text style={{marginTop: 5}}>Select Activity:</Text>
                <Picker
                    mode="dropdown"
                    iosHeader="Select Activity"
                    iosIcon={<Icon name="arrow-down"/>}
                    style={{width: undefined, backgroundColor: '#efefef'}}
                    selectedValue={this.state.selectedActivity}
                    onValueChange={(value) => this.setState({selectedActivity: value})}
                >
                {this.state.activities.map((item) => {
                    return (<Picker.Item key={item.id} label={item.name} value={item.id}/>)
                })}
                </Picker>

                <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 1, flexDirection: 'column'}}>
                        <Text style={{marginTop: 5}}>Temperature:</Text>
                        <Text style={styles.display_value}>{(this.state.temperature)}{'\xB0 F'}</Text>
                    </View>

                    <View style={{flex: 1, flexDirection: 'column'}}>
                        <Text style = {{marginTop: 5}}>Weather Conditions:</Text>
                        <Text style={styles.display_value}>{this.state.skyCondition}</Text>
                    </View>
                </View>
                
                <MapView
                    provider={PROVIDER_GOOGLE}
                    ref={map => {this.map = map}}
                    showsUserLocation={true}
                    style={styles.map}
                    initialRegion={{
                        latitude: 41.737,
                        longitude: -111.8338,
                        latitudeDelta: .01,
                        longitudeDelta: .01
                    }}
                >
                </MapView>

                <View style={{flex: 1, justifyContent: 'flex-end', marginBottom: 70}}>
                <Button large
                    style={styles.button}
                    onPress={() => this.props.navigation.navigate('During', {selectedActivity: this.state.selectedActivity, temperature: this.state.temperature, skyCondition: this.state.skyCondition})}
                >
                    <Text style={styles.button_text}>Start</Text>
                </Button>
                </View>

            </View>
        );
    }

    componentDidMount(){
        navigator.geolocation.getCurrentPosition(
            (position) => {
                this.map.animateCamera({center: {latitude: position.coords.latitude,
                    longitude: position.coords.longitude}});

                var weatherKey = '68a2b096a30ce4663d47ade5ee394f18';
                var weatherUrl = `https://api.openweathermap.org/data/2.5/weather?lat=${position.coords.latitude}&lon=${position.coords.longitude}&appid=${weatherKey}&units=imperial`;
                fetch(weatherUrl)
                    .then(function(response){
                        return response.json();
                    })
                    .then((wJson) => {
                        this.setState({
                            temperature: (wJson.main.temp).toFixed(),
                            skyCondition: wJson.weather[0].description
                        });
                    });


                this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    error: null,
                });
            },
            (error) => console.log("error: ", error.message ),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
        );
    }
}
