import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {Button} from 'native-base';
import styles from '../styles/Styles';
import haversine from 'haversine';
import moment from 'moment';

export default class DuringScreen extends Component {
    static navigationOptions = {
        title: 'Current Activity'
    }

    constructor(props) {
        super(props);

        this.state = {
            distance: 0,
            duration: 0,
            pace: 0,
            routeCoordinates: [],
            selectedActivity: this.props.navigation.state.params.selectedActivity,
            temperature: this.props.navigation.state.params.temperature,
            skyCondition: this.props.navigation.state.params.skyCondition,

            latitude: null,
            longitude: null,
            prevLatLong: {},

            textValue: 'Pause',
            paused: false
        }

        this.updateTimer();
    }

    render(){
        return(
            <View style={{flex: 1, flexDirection: 'column'}}>

                <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 1, flexDirection: 'column'}}>
                        <Text>Distance:</Text>
                        <Text style={styles.display_value}>{this.state.distance.toFixed(2)}</Text>
                        <Text>miles</Text>
                    </View>

                    <View style={{flex: 1, flexDirection: 'column'}}>
                        <Text>Duration:</Text>
                        <Text style={styles.display_value}>{moment().startOf('day').seconds(this.state.duration).format('mm:ss')}</Text>
                    </View>
                    
                    <View style={{flex: 1, flexDirection: 'column'}}>
                        <Text>Pace:</Text>
                        <Text style={styles.display_value}>{parseFloat(this.state.pace).toFixed(2)}</Text>
                        <Text>miles/min</Text>
                    </View>

                </View>

                <View
                    style={{
                        borderBottomColor: 'gray',
                        borderBottomWidth: 1,
                    }}
                />

                <Button large
                    style={styles.button}
                    onPress={() => this.pausePress()}
                >
                    <Text style={styles.button_text}>{this.state.textValue}</Text>
                </Button>

                <Button large
                    style={styles.button}
                    onPress={() => this.stopPress()}
                >
                    <Text style={styles.button_text}>Stop</Text>
                </Button>

            </View>
        );
    }

    componentDidMount() {
        this.gpsID = navigator.geolocation.watchPosition(
            (position) => {
                const latitude = position.coords.latitude;
                const longitude = position.coords.longitude;
                const newCoord = {
                    latitude,
                    longitude
                };
                if(this.state.paused == false){
                    this.setState({
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                        distance: this.state.distance + this.calcDistance(newCoord),
                        prevLatLong: newCoord,
                        routeCoordinates: this.state.routeCoordinates.concat([newCoord])
                    });
                }
            },
            (error) => console.log("Error:", error),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000, distanceFilter: 5 },
        );
    }

    componentWillUnmount() {
        navigator.geolocation.clearWatch(this.gpsID);
        clearInterval(this.timer);
    }

    calcDistance(newLatLong) {
        return haversine(this.state.prevLatLong, newLatLong, {unit: 'mile'}) || 0;
    }

    updateTimer(){
        this.timer = setInterval(() => {
            this.setState((prevState) => {
                return(
                    { duration: ++prevState.duration,
                      pace: this.state.distance / (this.state.duration / 60) }
                )
            });
        }, 1000);
    }

    pausePress(){
        if(this.state.paused == false){
            //Pause app
            clearInterval(this.timer);
            this.setState({
                textValue: 'Resume',
                paused: true
            });
        }
        else{
            //Resume app
            this.updateTimer();
            this.setState({
                textValue: 'Pause',
                paused: false
            });
        }
    }

    stopPress(){
        navigator.geolocation.clearWatch(this.gpsID);
        clearInterval(this.timer);
        this.props.navigation.navigate('After', {selectedActivity: this.state.selectedActivity, distance: this.state.distance, duration: this.state.duration, pace: this.state.pace, routeCoordinates: this.state.routeCoordinates, temperature: this.state.temperature, skyCondition: this.state.skyCondition});
    }
}