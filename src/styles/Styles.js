import { StyleSheet } from 'react-native';

export default StyleSheet.create({

    button: {
        alignSelf: 'center',
        justifyContent: 'center',
        width: 200,
        marginTop: 20
    },

    button_text: {
        fontSize: 20
    },

    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20,
    },

    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'black',
    },

    display_value: {
        fontSize: 40
    },

    item_container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignSelf: 'stretch',
    },

    map: {
        width: undefined,
        flexGrow: 1,
        height: 200,
        marginTop: 20
    },

    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },

    result_text: {
        alignSelf:'center',
    },
    
    reward: {
        margin: 10,
        backgroundColor: 'lightgray',
        padding: 5
    },

    textItem: {
        flex: 1,
        textAlign: 'center'
    },

});
