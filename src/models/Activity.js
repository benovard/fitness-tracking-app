
export default class Activity {

    constructor(date, type, duration, distance, pace, feeling, picture, routeCoordinates, beginTemp, beginSky, endTemp, endSky, photo){
        this.date = date;
        this.type = type;
        this.duration = duration;
        this.distance = distance;
        this.pace = pace;
        this.feeling = feeling;
        this.picture = picture;
        this.routeCoordinates = routeCoordinates;
        this.beginTemp = beginTemp;
        this.beginSky = beginSky;
        this.endTemp = endTemp;
        this.endSky = endSky;
        this.photo = photo;
    }
}