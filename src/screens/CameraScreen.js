import React, {Component} from 'react';
import {Text, View, TouchableOpacity, Platform, PermissionsAndroid, CameraRoll, ImageBackground} from 'react-native';
import { RNCamera } from 'react-native-camera';
import styles from '../styles/Styles';


export default class HistoryMenuScreen extends Component {
    static navigationOptions = {
        title: 'Camera'
    }

    constructor(props) {
        super(props);

        this.state = {
            imageUri: null,
            cameraType: 'back'
        }
    }

    render(){
        const imageUri = this.state.imageUri;
        console.log('uri:', imageUri);
        if(imageUri){
            return(
                <View style={{flex: 1}}>
                    <ImageBackground source={{uri: imageUri}} style={{flex: 1}}/>

                    <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center' }}>
                        <TouchableOpacity onPress={() => this.savePicture()} style={styles.capture}>
                            <Text style={{ fontSize: 14 }}>Save</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={styles.capture}>
                            <Text style={{ fontSize: 14 }}>Cancel</Text>
                        </TouchableOpacity>
                    </View>

                </View>
            )
        }
        return(
            <View style={{flex: 1}}>

                <TouchableOpacity onPress={() => this.flipCamera()} style={styles.capture}>
                    <Text style={{ fontSize: 14 }}>Flip Camera</Text>
                </TouchableOpacity>

                <RNCamera
                    ref={ref => {
                        this.camera = ref;
                    }}
                    style={styles.preview}
                    type={this.state.cameraType}
                    flashMode={RNCamera.Constants.FlashMode.on}
                    captureAudio={false}
                />

                <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => this.takePicture()} style={styles.capture}>
                        <Text style={{ fontSize: 14 }}>SNAP</Text>
                    </TouchableOpacity>
                </View>
                
            </View>
        );
    }

    componentDidMount() {
        if (Platform.OS === 'Android') {
            requestCameraRollPermission();
        }
    }

    takePicture() {
        if (this.camera) {
            const options = { quality: 0.5, base64: true };
            this.camera.takePictureAsync(options)
                .then(result => {
                    this.setState({
                        imageUri: result.uri
                    });
                    //CameraRoll.saveToCameraRoll(result.uri);    
                })
                .catch(error => console.log('error:', error));
        }
    };

    savePicture(){
        CameraRoll.saveToCameraRoll(this.state.imageUri);
        this.props.navigation.goBack();
    }

    flipCamera(){
        if (this.state.cameraType === 'back'){
            this.setState({
              cameraType: 'front'
            });
        } 
        else{
            this.setState({
              cameraType: 'back'
            });
        }
    }

}

async function requestCameraRollPermission() {
    try {
        const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
            {
                title: 'Save Photos',
                message:
                    'This App needs access to your camera roll to save pictures',
                buttonNegative: 'Deny',
                buttonPositive: 'Grant',
            },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            console.log('You can save pictures');
        } else {
            console.log('Camera roll access denied');
        }
    } catch (err) {
        console.warn(err);
    }
}