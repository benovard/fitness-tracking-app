import React, {Component} from 'react';
import {Text, View, ListView, FlatList} from 'react-native';
import {List, ListItem, Button, Icon, Picker} from 'native-base';
import dataController from '../services/datacontroller';
import Activity from '../models/Activity';
import DropdownMenu from 'react-native-dropdown-menu';
import styles from '../styles/Styles';


export default class HistoryMenuScreen extends Component {
    static navigationOptions = {
        title: 'Activity History'
    }

    constructor(props) {
        super(props);

        this.state = {
            activityList: {},
            displayList: [],
            text: '',

            selectedType: 'all',
            types: [{id: 'all', name: 'All'}, {id: 'walking', name: 'Walking'}, {id: 'running', name: 'Running'}, {id: 'biking', name: 'Biking'}],
            selectedSort: 'date',
            sorts: [{id: 'date', name: 'Date'}, {id: 'pace', name: 'Pace'}, {id: 'duration', name: 'Duration'}],
            selectedTime: 'week',
            times: [{id: 'week', name: 'Last Week'}, {id: 'month', name: 'Last Month'}, {id: 'year', name: 'Last Year'}]
        }

    }

    render(){
        return(
            <View>

              <FlatList
                data={Object.values(this.state.activityList)}
                renderItem={this._renderItem}
                style={{ alignSelf: 'stretch' }}
                keyExtractor={this._keyExtractor}
                ListHeaderComponent={this._renderHeader}
              >
              </FlatList>
                
            </View>
        );
    }

    componentDidMount() {
        dataController.getMany()
            .then(result => {
                this.setState({
                    activityList: result
                });
            })
            .catch(error => {
                console.log('addMany Error:', error);
            });
        //this.setDisplayList();
    }

    setDisplayList(){
        var list = [];
        var temp = Object.values(this.state.activityList);

        if(this.state.selectedType == 'all'){
            list = temp;
        }
        else{
            for (i of temp){
                if(i.type == this.state.selectedType){
                    list.concat(i);
                }
            }
        }
        console.log('list:', list);

        list.sort(this.dynamicSort("-" + this.state.selectedSort));
        this.setState({
            displayList: list
        })
    }

    dynamicSort(property) {
        var sortOrder = 1;
        if(property[0] === "-") {
            sortOrder = -1;
            property = property.substr(1);
        }
        return function (a,b) {
            var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
            return result * sortOrder;
        }
    }

    _keyExtractor = (item, index) => item.date

    _renderItem = ({ item }) => {
        return (
            <ListItem onPress={() => this.props.navigation.navigate('History', {activity: item, activityList: this.state.activityList})}>
                <Text style={styles.textItem}>{item.date}</Text>
                <Text style={styles.textItem}>{item.type}</Text>
                <Text style={styles.textItem}>{item.feeling}</Text>
            </ListItem>
        );
    }

    _renderHeader = () => {
        data = [["All", "Walking", "Running", "Biking"], ["Date", "Pace", "Duration"], ["Week", "Month", "Year"]];
        return(
            <View style={{flex: 1}}>

                <Picker
                    mode="dropdown"
                    iosHeader="Select Type"
                    iosIcon={<Icon name="arrow-down"/>}
                    style={{width: undefined, backgroundColor: '#efefef'}}
                    selectedValue={this.state.selectedType}
                    onValueChange={(value) => {
                        //this.setDisplayList();
                        this.setState({selectedType: value});
                    }}
                >
                {this.state.types.map((item) => {
                    return (<Picker.Item key={item.id} label={item.name} value={item.id}/>)
                })}
                </Picker>

                <Picker
                    mode="dropdown"
                    iosHeader="Sort By"
                    iosIcon={<Icon name="arrow-down"/>}
                    style={{width: undefined, backgroundColor: '#efefef'}}
                    selectedValue={this.state.selectedSort}
                    onValueChange={(value) => {
                        //this.setDisplayList();
                        this.setState({selectedSort: value})
                    }}
                >
                {this.state.sorts.map((item) => {
                    return (<Picker.Item key={item.id} label={item.name} value={item.id}/>)
                })}
                </Picker>

                <Picker
                    mode="dropdown"
                    iosHeader="Activities From"
                    iosIcon={<Icon name="arrow-down"/>}
                    style={{width: undefined, backgroundColor: '#efefef'}}
                    selectedValue={this.state.selectedTime}
                    onValueChange={(value) => this.setState({selectedTime: value})}
                >
                {this.state.times.map((item) => {
                    return (<Picker.Item key={item.id} label={item.name} value={item.id}/>)
                })}
                </Picker>
            </View>

        )
    }

}