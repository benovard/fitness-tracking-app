import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {Button, Picker, Icon} from 'native-base';
import MapView, {PROVIDER_GOOGLE, Callout, Marker, Polyline} from 'react-native-maps';
import styles from '../styles/Styles';
import moment from 'moment';

export default class HistoryScreen extends Component {
    static navigationOptions = {
        title: 'Past Activity'
    }

    constructor(props) {
        super(props);

        this.state = {
            activity: this.props.navigation.state.params.activity,
            activityList: this.props.navigation.state.params.activityList
        }
    }

    render(){
        return(
            <View style={{flex: 1, flexDirection: 'column'}}>
                <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 1, flexDirection: 'column'}}>
                        <Text>Distance:</Text>
                        <Text style={styles.display_value}>{this.state.activity.distance.toFixed(2)}</Text>
                        <Text>miles</Text>
                    </View>

                    <View style={{flex: 1, flexDirection: 'column'}}>
                        <Text>Duration:</Text>
                        <Text style={styles.display_value}>{moment().startOf('day').seconds(this.state.activity.duration).format('mm:ss')}</Text>
                    </View>
                    
                    <View style={{flex: 1, flexDirection: 'column'}}>
                        <Text>Pace:</Text>
                        <Text style={styles.display_value}>{parseFloat(this.state.activity.pace).toFixed(2)}</Text>
                        <Text>miles/min</Text>
                    </View>

                </View>

                <View
                    style={{
                        borderBottomColor: 'gray',
                        borderBottomWidth: 1,
                    }}
                />

                <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 1, flexDirection: 'column'}}>
                        <Text>Start Temperature:</Text>
                        <Text style={styles.display_value}>{this.state.activity.beginTemp}</Text>
                    </View>

                    <View style={{flex: 1, flexDirection: 'column'}}>
                        <Text>Start Conditions:</Text>
                        <Text style={{fontSize: 30}}>{this.state.activity.beginSky}</Text>
                    </View>
                    
                    <View style={{flex: 1, flexDirection: 'column'}}>
                        <Text>End Temperature:</Text>
                        <Text style={styles.display_value}>{this.state.activity.endTemp}</Text>
                    </View>
                    
                    <View style={{flex: 1, flexDirection: 'column'}}>
                        <Text>End Conditions:</Text>
                        <Text style={{fontSize: 30}}>{this.state.activity.endSky}</Text>
                    </View>
                </View>

                <View
                    style={{
                        borderBottomColor: 'gray',
                        borderBottomWidth: 1,
                    }}
                />      

                <MapView
                    provider={PROVIDER_GOOGLE}
                    ref={map => {this.map = map}}
                    showsUserLocation={true}
                    style={styles.map}
                    initialRegion={{
                        latitude: 41.0,
                        longitude: -111.0,
                        latitudeDelta: .02,
                        longitudeDelta: .02
                    }}
                >
                    <Polyline
                        coordinates={this.state.routeCoordinates}
                        strokeWidth={8}
                        strokeColor='#555'
                    />
                </MapView>

                <Button small
                    style={{...styles.button, marginBottom: 70}}
                    onPress={() => this.deletePress()}
                >
                    <Text style={styles.button_text}>Delete</Text>
                </Button>

            </View>
        );
    }

    componentDidMount(){
        navigator.geolocation.getCurrentPosition(
            (position) => {
                this.map.animateCamera({center: {latitude: this.state.activity.routeCoordinates[0].latitude,
                    longitude: this.state.activity.routeCoordinates[0].longitude}});
                this.setState({
                    coords: position
                });
            },
            (error) => console.log("error: ", error.message ),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
        );
    }

    deletePress(){
        
    }
}