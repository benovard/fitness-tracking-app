import React, {Component} from 'react';
import {View, Text} from 'react-native';
import MapView, {PROVIDER_GOOGLE, Callout, Marker, Polyline} from 'react-native-maps';
import {Button, Picker, Icon} from 'native-base';
import Activity from '../models/Activity';
import {StackActions, NavigationActions} from 'react-navigation';
import dataController from '../services/datacontroller';
import moment from 'moment';

import styles from '../styles/Styles';

export default class AfterScreen extends Component {
    static navigationOptions = {
        title: 'Activity Summary'
    }

    constructor(props) {
        super(props);

        this.state = {
            feelings: [{id: 'good', name: 'Good'}, {id: 'ok', name: 'Just Ok'}, {id: 'bad', name: 'Bad'}],
            selectedFeeling: 'good',
            distance: this.props.navigation.state.params.distance,
            duration: this.props.navigation.state.params.duration,
            pace: this.props.navigation.state.params.pace,
            routeCoordinates: this.props.navigation.state.params.routeCoordinates,
            selectedActivity: this.props.navigation.state.params.selectedActivity,
            coords: null,
            beginTemp: this.props.navigation.state.params.temperature,
            beginSky: this.props.navigation.state.params.skyCondition,
            endTemp: null,
            endSky: ''
        }
    }

    render(){
        return(
            <View style={{flex: 1, flexDirection: 'column'}}>

                <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 1, flexDirection: 'column'}}>
                        <Text>Distance:</Text>
                        <Text style={styles.display_value}>{this.state.distance.toFixed(2)}</Text>
                        <Text>miles</Text>
                    </View>

                    <View style={{flex: 1, flexDirection: 'column'}}>
                        <Text>Duration:</Text>
                        <Text style={styles.display_value}>{moment().startOf('day').seconds(this.state.duration).format('mm:ss')}</Text>
                    </View>
                    
                    <View style={{flex: 1, flexDirection: 'column'}}>
                        <Text>Pace:</Text>
                        <Text style={styles.display_value}>{parseFloat(this.state.pace).toFixed(2)}</Text>
                        <Text>miles/min</Text>
                    </View>
                </View>

                <View
                    style={{
                        borderBottomColor: 'gray',
                        borderBottomWidth: 1,
                    }}
                />

                <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 1, flexDirection: 'column'}}>
                        <Text>Start Temperature:</Text>
                        <Text style={styles.display_value}>{this.state.beginTemp}</Text>
                    </View>

                    <View style={{flex: 1, flexDirection: 'column'}}>
                        <Text>Start Conditions:</Text>
                        <Text style={{fontSize: 30}}>{this.state.beginSky}</Text>
                    </View>
                    
                    <View style={{flex: 1, flexDirection: 'column'}}>
                        <Text>End Temperature:</Text>
                        <Text style={styles.display_value}>{this.state.endTemp}</Text>
                    </View>
                    
                    <View style={{flex: 1, flexDirection: 'column'}}>
                        <Text>End Conditions:</Text>
                        <Text style={{fontSize: 30}}>{this.state.endSky}</Text>
                    </View>
                </View>

                <View
                    style={{
                        borderBottomColor: 'gray',
                        borderBottomWidth: 1,
                    }}
                />

                <Text style={{fontSize: 20, marginTop: 5}}>How do you feel?</Text>
                <Picker
                    mode="dropdown"
                    iosHeader="Select Activity"
                    iosIcon={<Icon name="arrow-down"/>}
                    style={{width: undefined, backgroundColor: '#efefef'}}
                    selectedValue={this.state.selectedFeeling}
                    onValueChange={(value) => this.setState({selectedFeeling: value})}
                >
                {this.state.feelings.map((item) => {
                    return (<Picker.Item key={item.id} label={item.name} value={item.id}/>)
                })}
                </Picker>

                <MapView
                    provider={PROVIDER_GOOGLE}
                    ref={map => {this.map = map}}
                    showsUserLocation={true}
                    style={styles.map}
                    initialRegion={{
                        latitude: 41.737,
                        longitude: -111.8338,
                        latitudeDelta: .01,
                        longitudeDelta: .01
                    }}
                >
                    <Polyline
                        coordinates={this.state.routeCoordinates}
                        strokeWidth={8}
                        strokeColor='#555'
                    />
                </MapView>

                <View style={{flex: 1, justifyContent: 'flex-end', marginBottom: 20}}>
                    <Button small
                        style={styles.button}
                        onPress={() => this.savePress()}
                    >
                        <Text style={styles.button_text}>Save</Text>
                    </Button>

                    <Button small
                        style={styles.button}
                        onPress={() => this.deletePress()}
                    >
                        <Text style={styles.button_text}>Delete</Text>
                    </Button>

                    <Button small
                        style={styles.button}
                        onPress={() => this.props.navigation.navigate('Camera')}
                    >
                        <Text style={styles.button_text}>Take Picture</Text>
                    </Button>
                </View>


            </View>
        );
    }

    componentDidMount(){
        navigator.geolocation.getCurrentPosition(
            (position) => {
                this.map.animateCamera({center: {latitude: position.coords.latitude,
                    longitude: position.coords.longitude}});
                this.setState({
                    coords: position
                });

                var weatherKey = '68a2b096a30ce4663d47ade5ee394f18';
                var weatherUrl = `https://api.openweathermap.org/data/2.5/weather?lat=${position.coords.latitude}&lon=${position.coords.longitude}&appid=${weatherKey}&units=imperial`;
                fetch(weatherUrl)
                    .then(function(response){
                        return response.json();
                    })
                    .then((wJson) => {
                        this.setState({
                            endTemp: (wJson.main.temp).toFixed(),
                            endSky: wJson.weather[0].description
                        });
                    });
            },
            (error) => console.log("error: ", error.message ),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
        );
    }

    reset(){
        return this.props.navigation.dispatch(StackActions.reset({
            index: 0, actions: [NavigationActions.navigate({routeName: 'Home'})]
        }));
    }

    savePress(){
        var date = new Date();
        var dateString = date.toString();
       activity = new Activity(dateString, this.state.selectedActivity, this.state.duration, this.state.distance, this.state.pace, this.state.selectedFeeling, 'picture goes here', this.state.routeCoordinates, this.state.beginTemp, this.state.beginSky, this.state.endTemp, this.state.endSky);
       dataController.addMany({
            [dateString]: activity
       })
       .then(() => dataController.getMany())
       .then(result => {
           console.log(result);
       });
       this.reset();
    }

    deletePress(){
        //Clear stack
        this.reset();
    }
}