import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import {createAppContainer, createStackNavigator} from 'react-navigation';
import StartScreen from './src/screens/StartScreen';
import RewardsScreen from './src/screens/RewardsScreen';
import HistoryMenuScreen from './src/screens/HistoryMenuScreen';
import HistoryScreen from './src/screens/HistoryScreen';
import DuringScreen from './src/screens/DuringScreen';
import AfterScreen from './src/screens/AfterScreen';
import HomeScreen from './src/screens/HomeScreen';
import CameraScreen from './src/screens/CameraScreen';


export default class App extends Component {
  render() {
    return (
      <AppContainer/>
    );
  }
}

const Root = createStackNavigator(
  {
    Home: HomeScreen,
    Start: StartScreen,
    Rewards: RewardsScreen,
    HistoryMenu: HistoryMenuScreen,
    History: HistoryScreen,
    During: DuringScreen,
    After: AfterScreen,
    Camera: CameraScreen
  },
  {
    initialRouteName: 'Home'
  }
);

const AppContainer = createAppContainer(Root);


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
