import React, {Component} from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import dataController from '../services/datacontroller';
import styles from '../styles/Styles'

export default class RewardsScreen extends Component {
    static navigationOptions = {
        title: 'Rewards'
    }

    constructor(props) {
        super(props);

        this.state = {
            activities: null,
            bestPace: {},
            activityCount: 0,
            goodAct: 0,
            okAct: 0,
            badAct: 0
        }
    }

    render(){
        return(
            <View style={{flex:1, flexDirection: 'column'}}>
                <View style={styles.reward}>
                    <Text>You are level:</Text>
                    <Text style={styles.display_value}> {Math.floor(this.state.activityCount / 5)} </Text>
                    <Text>Complete more workouts to level up!</Text>
                </View>

                <TouchableOpacity
                    style={styles.reward}
                    onPress={() => this.props.navigation.navigate('History', {activity: this.state.bestPace})}
                >
                    <Text>Your best pace is:</Text>
                    <Text style={{...styles.display_value, color: 'blue'}}>{this.state.bestPace.pace}</Text>
                </TouchableOpacity>

                <View style={styles.reward}>
                    <View style={{flexDirection: 'row'}}>
                        <View style={{flex: 1, flexDirection: 'column'}}>
                            <Text style={{marginTop: 5}}>Good Activities:</Text>
                            <Text style={styles.display_value}>{(this.state.goodAct)}</Text>
                        </View>

                        <View style={{flex: 1, flexDirection: 'column'}}>
                            <Text style = {{marginTop: 5}}>OK Activities:</Text>
                            <Text style={styles.display_value}>{this.state.okAct}</Text>
                        </View>

                        <View style={{flex: 1, flexDirection: 'column'}}>
                            <Text style = {{marginTop: 5}}>Bad Activities:</Text>
                            <Text style={styles.display_value}>{this.state.badAct}</Text>
                        </View>
                    </View>
                </View>

            </View>
        );
    }

    componentDidMount() {
        dataController.getMany()
            .then(result => {
                var list = Object.values(result);
                var res = Math.max.apply(Math, list.map(function(o){return o.pace;}));
                var obj = list.find(function(o){ return o.pace == res; });

                var good = 0;
                var ok = 0;
                var bad = 0;
                for (a of list){
                    switch(a.feeling){
                        case 'good':
                            good++;
                            break;
                        case 'ok':
                            ok++;
                            break;
                        case 'bad':
                            bad++;
                            break;
                    }
                }
                this.setState({
                    activities: list,
                    activityCount: list.length,
                    bestPace: obj,
                    goodAct: good,
                    okAct: ok,
                    badAct: bad
                });
            })
            .catch(error => {
                console.log('addMany Error:', error);
            })
    }
}