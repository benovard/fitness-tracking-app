import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {Button} from 'native-base';
import styles from '../styles/Styles';


export default class HomeScreen extends Component {
    static navigationOptions = {
        title: 'Home'
    }

    constructor(props) {
        super(props);

        this.state = {
        }
    }

    render(){
        return(
            <View style={{flex: 1, flexDirection: 'column'}}>
                <Button large
                    style={styles.button}
                    onPress={() => this.props.navigation.navigate('Start')}
                >
                    <Text style={styles.button_text}>New Activity</Text>
                </Button>

                <Button large
                    style={styles.button}
                    onPress={() => this.props.navigation.navigate('HistoryMenu')}
                >
                    <Text style={styles.button_text}>Activity History</Text>
                </Button>

                <Button large
                    style={styles.button}
                    onPress={() => this.props.navigation.navigate('Rewards')}
                >
                    <Text style={styles.button_text}>Rewards</Text>
                </Button>
                

            </View>
        );
    }
}