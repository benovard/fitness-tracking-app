import localStorage from '../services/localstorage'

class DataController {

    addItem(key, value) {
        return new Promise((resolve, reject) => {
            localStorage.add(key, value)
            .then(() => {
                resolve();
            })
            .catch(error => {
                console.log(error)
            })
        })
    }

    addMany(value) {
        return localStorage.merge('manyItems', value);
    }

    getItem(key) {
        return new Promise((resolve, reject) => {
            localStorage.get(key)
            .then(result => {
                resolve(result);
            })
            .catch(error => reject(error))
        })
    }

    getMany() {
        return new Promise((resolve, reject) => {
            localStorage.get('manyItems')
            .then(result => {
                resolve(result);
            })
            .catch(error => reject(error))
        })
    }
}

const dataController = new DataController();
export default dataController;